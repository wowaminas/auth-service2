package com.marketplace.authservice.controller;

import com.marketplace.authservice.config.jwt.JWTProvider;
import com.marketplace.authservice.config.security.UserEntityDetailsService;
import com.marketplace.authservice.dto.AuthRequestDto;
import com.marketplace.authservice.dto.AuthResponseDto;
import com.marketplace.authservice.resource.AuthResource;
import com.marketplace.authservice.service.UserService;

import com.marketplace.authservice.util.exceptions.AccessDeniedException;
import com.marketplace.authservice.util.exceptions.UserAlreadyExistException;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RestController;


@AllArgsConstructor
@RestController
public class AuthController implements AuthResource {
    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final JWTProvider jwtProvider;
    private final UserEntityDetailsService userEntityDetailsService;



    @Override
    public ResponseEntity<String> signIn(AuthRequestDto user) {
        if (!userService.existsByUsername(user)) {
            userService.register(user);
            return ResponseEntity.ok("User successfully registered");
        } else {
            throw new UserAlreadyExistException("User " + user.getUsername() + " already exists");
        }
    }

    @Override
    public ResponseEntity<AuthResponseDto> login(AuthRequestDto loginDto) {
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword(),
                           userEntityDetailsService.getAuthorities(loginDto.getRole()))
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String token = jwtProvider.generateToken(authentication);
            return new ResponseEntity<>(new AuthResponseDto(token), HttpStatus.OK);
        } catch (AuthenticationException ex) {
            throw new AccessDeniedException(ex.getMessage() + " " + loginDto.getUsername());
        }
    }
}
