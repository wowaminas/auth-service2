package com.marketplace.authservice.entity;

import org.springframework.security.core.GrantedAuthority;

/**
 * Перечисление ролей.
 */
public enum RoleEnum implements GrantedAuthority {
    SELLER("Продавец"),
    CUSTOMER("Клиент"),
    COURIER("Курьер");
    private final String roleName;

    RoleEnum(String roleName) {
        this.roleName = roleName;
    }

    @Override
    public String toString() {
        return this.roleName;
    }

    @Override
    public String getAuthority() {
        return "ROLE_" + name();
    }
}
