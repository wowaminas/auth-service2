package com.marketplace.authservice.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Сущность пользователя.
 * Чтобы не иметь конфликтов пересечения имен классов с уже имеющимися классами в Spring Security, назвал сущность UserEntity для удобства.
 */

@Entity
@Table(name = "users")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;
    @NotEmpty
    @Column
    private String username;
    @NotEmpty
    @Column
    private String password;
    @Column(name = "roles")
    @Enumerated(EnumType.STRING)
    private RoleEnum role;
}