package com.marketplace.authservice.dto;

import com.marketplace.authservice.entity.RoleEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;


/**
 * DTO объект, использующийся для регистрации и логина в приложении.
 */
@Data
@AllArgsConstructor
@Schema(description = "Дто для аутентификации")
public class AuthRequestDto {

    @NonNull
    private String username;
    @NonNull
    private String password;
    @NonNull
    private RoleEnum role;

}
